# Rolling Release

## Examples

### Create a service in a Swarm

Throughout the process, you can check the current state of the cluster and the existing nodes in the web interface in the menu 'services' and 'nodes'.

    docker network create -d overlay consul
    docker service create --name consul \
      --publish 8500:8500 \
      --network consul \
      --replicas 3 \
      --restart-condition any \
      --update-order stop-first \
      --update-delay 120s \
      --update-parallelism 1 \
      registry.gitlab.com/epicdocker/consul:latest

Once the cluster has been initialized, the web interface `http://localhost:8500/` can be opened in the browser and see the nodes in the cluster.

#### Add Key/Value pairs 

To test you can add some Key/Value pairs via the web interface, so that a dataset is simmuliert.

#### Update the service

    docker service update --force consul

#### Scale up 

    docker service scale consul=5

#### Update the service (for the second time)

    docker service update --force consul
    
#### Validate Key/Value pairs 

Check in the web interface if the entered Key/Value pairs still exist in the consul store.

#### Clean up

    docker service rm consul
    docker network rm consul
